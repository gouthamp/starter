(function() {
    'use strict';

    angular
        .module('starterApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
