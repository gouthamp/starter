package com.rs.starter.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH,ENGLISH,SPANISH
}
