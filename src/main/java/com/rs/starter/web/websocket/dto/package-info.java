/**
 * Data Access Objects used by WebSocket services.
 */
package com.rs.starter.web.websocket.dto;
