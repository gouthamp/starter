/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.rs.starter.web.rest.dto;
